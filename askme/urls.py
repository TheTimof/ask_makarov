from django.conf.urls import url
from django.urls import path
from django.contrib import admin
from app.views import *
from django.conf import settings as django_settings
from django.conf.urls.static import static

from app import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('hot/', views.hot, name='hot'),
    path('login/', views.login_page, name='login'),
    path('logout/', views.logout_page, name='logout'),
    path('registration/', views.registration, name='registration'),
    path('ask/', views.ask, name='ask'),
    path('tag/<int:tag_id>', views.tag, name='tag'),
    path('settings/', views.settings, name='settings'),
    path('question/<int:question_id>', views.question, name='question'),
] + static(django_settings.MEDIA_URL, document_root=django_settings.MEDIA_ROOT)
