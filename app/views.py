from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from .models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib import messages

def paginate(objects, request, amount):
    paginator = Paginator(objects, amount)
    page = request.GET.get('page')
    objects_page = paginator.get_page(page)
    return objects_page

def index(request):
    newest_questions = Question.objects.new()
    context = {
        'questions': paginate(newest_questions, request, 5),
    }
    return render(request, 'index.html', context)

def hot(request):
    hottest_questions = Question.objects.top()
    context = {
        'questions': paginate(hottest_questions, request, 5),
    }
    return render(request, 'hot.html', context)

def login_page(request):
    form = None
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST, files=request.FILES)
        print(form.errors)
        if form.is_valid():
            user = form.get_user()
            if user is not None:
                login(request, user)
                return redirect('index')
        else:
            messages.error(request, 'username or password not correct')
    if request.method == 'GET':
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

def logout_page(request):
    logout(request)
    return redirect('login')

def registration(request):
    if request.method == 'POST':
        form = SignupForm(data=request.POST, files=request.FILES)
        print(form.errors)
        if form.is_valid():
            print("valid")
            user = form.save(commit=False)
            user.set_password(user.password)
            user.save()
            return redirect('login')

    form = SignupForm()
    context = {
        'form': form
    }
    return render(request, 'registration.html', context)

@login_required(login_url='login')
def ask(request):
    if (request.method == 'POST'):
        form = AskForm(data=request.POST)
        if form.is_valid:
            question = form.save(commit=False)
            question.author = request.user
            question.save()
            for tag in form['tag_list'].data.split():
                new_tag = Tag.objects.get_or_create(name=tag)
                print(new_tag)
                question.tag.add(new_tag[0])
            question.save()
            return redirect('question', question.id)

    if (request.method == 'GET'):
        form = AskForm()
    return render(request, 'ask.html', {'form': form} )

def tag(request, tag_id):
    newest_questions = Question.objects.tag(tag_id)
    context = {
        'questions': paginate(newest_questions, request, 5),
        'tag': Tag.objects.get(id=tag_id)
    }
    return render(request, 'tag.html', context)

@login_required(login_url='login')
def settings(request):
    form = None
    if (request.method == 'POST'):
        form = SettingsForm(data=request.POST, files=request.FILES, instance=request.user)
        if form.is_valid:
            user = form.save(commit=False)
            user.save()
        return render(request, 'settings.html', {'form': form})
    if (request.method == 'GET'):
        form = SettingsForm(instance=request.user)

    return render(request, 'settings.html', {'form': form})

def question(request, question_id):
    if (request.method == 'POST'):
        form = AnswerForm(data=request.POST)
        if form.is_valid:
            answer = form.save(commit=False)
            answer.author = request.user
            answer.question = Question.objects.get(id=question_id)
            answer.save()
            return redirect('question', question_id)

    form = AnswerForm()
    answers = Answer.objects.answer(question_id)
    context = {
        'form': form,
        'answers': paginate(answers, request, 3),
        'question': Question.objects.get(id=question_id)
    }


    return render(request, 'question.html', context)



